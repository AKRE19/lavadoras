<%@page contentType="text/html" pageEncoding="UTF-8"
        
import ="java.sql.Connection"        
import ="java.sql.DriverManager"        
import ="java.sql.ResultSet"        
import ="java.sql.Statement"        
import ="java.sql.SQLException"

%>

<%
    
    Class.forName("com.mysql.cj.jdbc.Driver");
    Connection conex = (Connection)DriverManager.getConnection("jdbc:mysql://127.0.0.1/lavar?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC&allowPublicKeyRetrieval=true&useSSL=false","root","osamenor19");
    Statement sql = conex.createStatement();
    String qry = "select * from lavadoras where visibilidad=1";
    ResultSet data = sql.executeQuery(qry);
    
%>



<!DOCTYPE html>
<html>
    
    <head>
        
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Historial</title>
        
    </head>
    
    <body style="background-color:#91F6FA;">
        
        <h1 style="color:#000000"><center>Historial</center></h1>
        
        <table style="width:100%; border:1px solid white; border-collapse:collapse ; align-content:flex-start">
            
            <tr>
                <th style="color:#000000; border:1px solid white;">id</th>
                <th style="color:#000000; border:1px solid white;">Descripción</th>
                <th style="color:#000000; border:1px solid white;">Precio</th>
                <th style="color:#000000; border:1px solid white;">Fecha</th>
            </tr>
            <%
                while (data.next()){
            %>
            <tr>
                <td style="color:#000000; border:1px solid white;"><%= data.getString("id")%></td>
                <td style="color:#000000; border:1px solid white;"><%= data.getString("texto")%></td>
                <td style="color:#000000; border:1px solid white;"><%= data.getString("calificacion")%></td>
                <td style="color:#000000; border:1px solid white;"><%= data.getString("fecha")%></td>
            </tr>
            <%
                }
                data.close();
            %>
            
        </table>
        
        <a style="font-size:20px; color:#000000" href="index.html">Regresar</a>
        
    </body>
    
</html>
